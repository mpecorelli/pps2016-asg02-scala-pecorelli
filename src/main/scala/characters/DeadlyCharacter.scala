package characters

import component.GameComponent
import java.awt._

/**
  * Created by margherita on 15/03/17.
  */
abstract class DeadlyCharacter(posX: Int, posY: Int, wid: Int, heig: Int, val img: Image) extends Character(posX,posY,wid,heig) with Runnable {
  private val PAUSE = 15
  private val image: Image = img

  override def moving_= (mov: Boolean): Unit = {
    super.moving_=(mov)
    if (!moving) movingRight = false
  }

  override def shift(): Unit = x_= (x + (if (movingRight) 1 else -1));

  override def run(): Unit = while ({
    true
  }) if (alive) {
    this.shift()
    try Thread.sleep(PAUSE)
    catch {
      case e: InterruptedException =>
    }
  }

  def changeImageAfterDeath: Image
}