package characters

import java.awt.Image

import component._
import objects.{GameObject, ObjectInterchangeable}
import utils.Res

trait GameCharacter extends GameComponent {

  def alive: Boolean

  def alive_= (isAlive: Boolean): Unit

  def movingRight: Boolean

  def movingRight_= (isMovingRight: Boolean): Unit

  def moving: Boolean

  def moving_= (isMoving: Boolean): Unit

  def changeImage(name: String, frequency: Int): Image

  def isCloseToAComponent(component: GameComponent): Boolean

  def contactWithComponent(component: GameComponent): Unit

  def isInContactWithANInterchangeableObject(objectInterchangeable: ObjectInterchangeable): Boolean
}

object Character {
  private val PROXIMITY_MARGIN = 10
  private val MOVING_MARGIN = 5
}

import Character._
case class Character(posX: Int, posY: Int, wid: Int, heig: Int) extends Component(posX, posY, wid, heig) with GameCharacter {
  private var isMoving = false
  private var isMovingRight = false
  private var counter = 0
  private var isAlive = true

  override def alive = isAlive

  override def alive_= (alive: Boolean) = isAlive = alive

  override def movingRight = isMovingRight

  override def movingRight_= (movingRight: Boolean) = isMovingRight = movingRight

  override def moving = isMoving

  override def moving_= (moving: Boolean) =isMoving = moving

  override def changeImage(name: String, frequency: Int): Image = {
    val path = Res.IMG_BASE + name + (if (!this.isMoving || {
      this.counter += 1
      this.counter
    } % frequency == 0) Res.IMGP_STATUS_ACTIVE else Res.IMGP_STATUS_NORMAL) +
    (if (this.isMovingRight) Res.IMGP_DIRECTION_DX else Res.IMGP_DIRECTION_SX) +
    Res.IMG_EXT
    utils.Utils.getImage(path)
  }

  override def contactWithComponent(component: GameComponent): Unit =
  if (isGoingRight(component) && isMovingRight) isMovingRight = false
  else if (isGoingLeft(component) && !isMovingRight) isMovingRight = true

  override def isInContactWithANInterchangeableObject(objectInterchangeable: ObjectInterchangeable): Boolean = isGoingLeft(objectInterchangeable) ||
    isMovingUp(objectInterchangeable) || isGoingRight(objectInterchangeable) || isMovingDown(objectInterchangeable)

  override def isCloseToAComponent(component: GameComponent): Boolean = (x > component.x - PROXIMITY_MARGIN && x < component.x + component.width + PROXIMITY_MARGIN) ||
    (x + width > component.x - PROXIMITY_MARGIN && x + width < component.x + component.width + PROXIMITY_MARGIN)

  protected def isGoingRight(component: GameComponent): Boolean = {
    if (component.isInstanceOf[Character] && !isMovingRight) return false
    !(x + width < component.x || x + width > component.x + MOVING_MARGIN || y + height <= component.y || y >= component.y + component.height)
  }

  protected def isGoingLeft(component: GameComponent): Boolean = !(x > component.x + component.width ||
    x + width < component.x + component.width - MOVING_MARGIN || y + height <= component.y || y >= component.y + component.height)

  protected def isMovingDown(component: GameComponent): Boolean = {
    var i = 0
    if (component.isInstanceOf[GameObject]) i = MOVING_MARGIN
    !(x + width < component.x + i || x > component.x + component.width - i || y + height < component.y || y + height > component.y + i)
  }

  protected def isMovingUp(component: GameComponent): Boolean = !(x + width < component.x + MOVING_MARGIN ||
    x > component.x + component.width - MOVING_MARGIN || y < component.y + component.height || y > component.y + component.height + MOVING_MARGIN)

}
