package characters

import java.awt.Image

import component.GameComponent
import game.Main
import utils.{Res, Utils}

object Mario {
  private val MARIO_OFFSET_Y_INITIAL = 243
  private val FLOOR_OFFSET_Y_INITIAL = 293
  private val WIDTH = 28
  private val HEIGHT = 50
  private val JUMPING_LIMIT = 42
}

import Mario._
class Mario (posX: Int, posY: Int) extends Character(posX, posY, WIDTH, HEIGHT) {

  private var imgMario =  Utils.getImage(Res.IMG_MARIO_DEFAULT)
  private var jumpingExtent = 0
  private var isJumping = false

  def jumping: Boolean =  isJumping

  def jumping_= (jump: Boolean) = isJumping = jump

  def image: Image = imgMario

  def jump: Image = {
    var str = ""
    jumpingExtent = jumpingExtent + 1

    if (jumpingExtent < JUMPING_LIMIT) {
      if (y > Main.platform.heightLimit) y = (y - 4)
      else jumpingExtent = JUMPING_LIMIT
      str = if (movingRight) Res.IMG_MARIO_SUPER_DX else Res.IMG_MARIO_SUPER_SX
    }
    else if (y + height < Main.platform.floorOffsetY) {
      y = (y + 1)
      str = if (movingRight) Res.IMG_MARIO_SUPER_DX else Res.IMG_MARIO_SUPER_SX
    }
    else {
      str = if (movingRight) Res.IMG_MARIO_ACTIVE_DX else Res.IMG_MARIO_ACTIVE_SX
      jumping_=(false)
      this.jumpingExtent = 0
    }

    Utils.getImage(str)
  }

  override def contactWithComponent(component: GameComponent): Unit = {
    if (component.isInstanceOf[Character]) {
      val character = component.asInstanceOf[Character]
      if (isGoingRight(character) || isGoingLeft(character)) {
        if (character.alive) {
          moving = false
          alive = false
        } else alive = true
      }
      else if (isMovingDown(character)) {
        character.moving = false
        character.movingRight = false
        character.alive = false
      }
    }
    if (isGoingRight(component) && movingRight || isGoingLeft(component) && !movingRight) {
      Main.platform.mov = 0
      this.moving = false
    }
    if (isMovingDown(component) && jumping) Main.platform.floorOffsetY = component.y
    else if (!isMovingDown(component)) {
      Main.platform.floorOffsetY = FLOOR_OFFSET_Y_INITIAL
      if (!jumping) y_$eq(MARIO_OFFSET_Y_INITIAL)
      if (isMovingUp(component)) Main.platform.heightLimit = (component.y + component.height) else if (!isMovingUp(component) && !jumping) Main.platform.heightLimit = 0
    }
  }

}