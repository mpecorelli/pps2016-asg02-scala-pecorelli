package characters

import java.awt.Image
import utils.Res
import utils.Utils

object Mushroom {
  private val WIDTH = 27
  private val HEIGHT = 30
}

class Mushroom(posX: Int,posY: Int) extends DeadlyCharacter(posX, posY, Mushroom.WIDTH, Mushroom.HEIGHT, Utils.getImage(Res.IMG_MUSHROOM_DEFAULT)) {
  val chronoMushroom = new Thread(this)
  chronoMushroom.start()

  override def changeImageAfterDeath: Image = Utils.getImage(if (this.movingRight) Res.IMG_MUSHROOM_DEAD_DX else Res.IMG_MUSHROOM_DEAD_SX)
}