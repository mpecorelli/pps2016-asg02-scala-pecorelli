package characters

import java.awt.Image
import utils.Res
import utils.Utils

object Turtle {
  private val WIDTH = 43
  private val HEIGHT = 50
}

class Turtle(posX: Int,posY: Int) extends DeadlyCharacter(posX, posY, Turtle.WIDTH, Turtle.HEIGHT, Utils.getImage(Res.IMG_TURTLE_IDLE)) {
  val chronoTurtle = new Thread(this)
  chronoTurtle.start()

  override def changeImageAfterDeath: Image = Utils.getImage(Res.IMG_TURTLE_DEAD)
}