package component

import game.Main

/**
  * Created by margherita on 15/03/17.
  */
trait GameComponent {

  def width: Int

  def height: Int

  def x: Int

  def y: Int

  def x_= (x: Int): Unit

  def y_= (y: Int): Unit

  def shift(): Unit
}

class Component(override var x: Int, override var y: Int, override val width: Int, override val height: Int) extends GameComponent {

  override def shift(): Unit = if (Main.platform.xPos >= 0) x = (x - Main.platform.mov)
}