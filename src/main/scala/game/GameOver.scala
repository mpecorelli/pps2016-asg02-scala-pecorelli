package game

import java.awt.Graphics
import javax.swing.JPanel

import utils.{Res, Utils}

/**
  * Created by margherita on 12/04/17.
  */
class GameOver extends JPanel{

  override def paint(graphics: Graphics) = graphics drawImage (Utils.getImage(Res.IMG_GAME_OVER),0,0,null)
}
