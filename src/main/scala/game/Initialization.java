package game;

import characters.DeadlyCharacter;
import characters.Mushroom;
import characters.Turtle;
import objects.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.BiFunction;

/**
 * Created by margherita on 11/04/17.
 */
public class Initialization {

    private static final ResourceBundle INTRO_LABELS = ResourceBundle.getBundle("resources.Initialization");

    private int nOfMushroom = howMany("mush");
    private int nOfTurtle = howMany("turt");
    private int nOfTunnel = howMany("tunn");
    private int nOfBlock = howMany("bloc");
    private int nOfPiece = howMany("piec");
    private final List<GameObject> objects = new ArrayList<>();
    private final List<ObjectInterchangeable> interchangeablesObject = new ArrayList<>();
    private final List<DeadlyCharacter> characters = new ArrayList<>();

    public Initialization() {
        addToList(nOfMushroom, "mushroom", characters, Mushroom::new);
        addToList(nOfTurtle, "turtle", characters, Turtle::new);
        addToList(nOfTunnel, "tunnel", objects, Tunnel::new);
        addToList(nOfBlock, "block", objects, Block::new);
        addToList(nOfPiece, "piece", interchangeablesObject, Piece::new);
    }

    private static <T>  void addToList(final int nOfComponents, final String componentType, final List<? super T> list, BiFunction<Integer, Integer, T> constructor) {
        for(int i = 1; i <= nOfComponents; i++) {
            list.add(constructor.apply(Integer.parseInt(INTRO_LABELS.getString(componentType+i+"x")), Integer.parseInt(INTRO_LABELS.getString(componentType+i+"y"))));
        }
    }

    private static int howMany(final String typeOfComponent) {
        return (int) Collections.list(INTRO_LABELS.getKeys()).stream().filter(x -> x.startsWith(typeOfComponent)).count() / 2;
    }

    public List<GameObject> getObjects() {
        return objects;
    }

    public List<ObjectInterchangeable> getInterchangeablesObject() {
        return interchangeablesObject;
    }

    public List<DeadlyCharacter> getCharacters() {
        return characters;
    }

    public int getNOfMushroom() {
        return this.nOfMushroom;
    }

    public int getNOfTurtle() {
        return this.nOfTurtle;
    }

    public int getNOfTunnel() {
        return this.nOfTunnel;
    }

    public int getNOfBlock() {
        return this.nOfBlock;
    }

    public int getNOfPiece() {
        return this.nOfPiece;
    }
}
