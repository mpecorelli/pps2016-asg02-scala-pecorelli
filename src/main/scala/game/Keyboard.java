package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    @Override
    public void keyPressed(KeyEvent e) {

        if (Main.platform.mario().alive() == true) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                // per non fare muovere il castello e start
                if (Main.platform.xPos() == -1) {
                    Main.platform.xPos_$eq(0);
                    Main.platform.background1PosX(-50);
                    Main.platform.background2PosX(750);
                }
                Main.platform.mario().moving_$eq(true);
                Main.platform.mario().movingRight_$eq(true);
                Main.platform.mov_$eq(1);
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                if (Main.platform.xPos() == 4601) {
                    Main.platform.xPos_$eq(4600);
                    Main.platform.background1PosX(50);
                    Main.platform.background2PosX(750);
                }
                Main.platform.mario().moving_$eq(true);
                Main.platform.mario().movingRight_$eq(false);
                Main.platform.mov_$eq(-1); // si muove verso destra
            }
            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                Main.platform.mario().jumping_$eq(true);
                Audio.playSound("/resources/audio/jump.wav");
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Main.platform.mario().moving_$eq(false);
        Main.platform.mov_$eq(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
