package game

import component.GameComponent
import characters._
import java.awt.Graphics
import java.awt.Image
import java.util.Observable
import javax.swing.JPanel

import utils.Res
import utils.Utils

object Platform {
  private val MARIO_FREQUENCY = 25
  private val MUSHROOM_FREQUENCY = 45
  private val TURTLE_FREQUENCY = 45
  private val MUSHROOM_DEAD_OFFSET_Y = 20
  private val TURTLE_DEAD_OFFSET_Y = 30
  private val FLAG_X_POS = 4650
  private val CASTLE_X_POS = 4850
  private val FLAG_Y_POS = 115
  private val CASTLE_Y_POS = 145
}

import Platform._
class Platform() extends JPanel {
  private[this] var _background1PosX = -50
  private[this] var _background2PosX = 750
  private[this] var _mov = 0
  private[this] var _xPos = -1
  private[this] var _floorOffsetY = 293
  private[this] var _heightLimit = 0
  private[this] val imageBackground1: Image = Utils.getImage(Res.IMG_BACKGROUND)
  private[this] val imageBackground2: Image = Utils.getImage(Res.IMG_BACKGROUND)
  private[this] val imageCastle: Image = Utils.getImage(Res.IMG_CASTLE)
  private[this] val imageStart: Image = Utils.getImage(Res.START_ICON)
  private[this] val _mario: Mario = new Mario(300, 245)
  private[this] val imgCastle: Image = Utils.getImage(Res.IMG_CASTLE_FINAL)
  private[this] val imgFlag: Image = Utils.getImage(Res.IMG_FLAG)
  private[this] val initialization = new Initialization
  private[this] val characters = initialization.getCharacters
  private[this] val objects = initialization.getObjects
  private[this] var interchangeablesObject = initialization.getInterchangeablesObject
  private[this] val nOfMushroom = initialization.getNOfMushroom
  private[this] val nOfTurtle = initialization.getNOfTurtle
  private[this] val nOfTunnel = initialization.getNOfTunnel
  private[this] val nOfBlock = initialization.getNOfBlock
  private[this] val nOfPiece = initialization.getNOfPiece


  setFocusable(true)
  requestFocusInWindow
  addKeyListener(new Keyboard)


  def mario: Mario = _mario

  def floorOffsetY: Int = _floorOffsetY

  def floorOffsetY_=(floorOffsetY: Int) = _floorOffsetY = floorOffsetY

  def heightLimit: Int = _heightLimit

  def heightLimit_=(heightLimit: Int) = _heightLimit = heightLimit

  def mov: Int = _mov

  def mov_=(mov: Int) = _mov = mov

  def xPos: Int = _xPos

  def xPos_=(xPos: Int) = _xPos = xPos

  def background1PosX(background1PosX: Int) = _background1PosX = background1PosX

  def background2PosX(background2PosX: Int) = _background2PosX = background2PosX

  def updateBackgroundOnMovement(): Unit = {
    if (_xPos >= 0 && _xPos <= 4600) {
      xPos_=(_xPos + mov)
      background1PosX(_background1PosX - mov)
      background2PosX(_background2PosX - mov)
    }
    _background1PosX match {
      case -800 => _background1PosX = 800
      case 800 => _background1PosX = -800
      case _ => _background2PosX match {
                  case -800 => _background2PosX = 800
                  case 800 => _background2PosX = -800
                  case _ =>
                }
    }
  }

  private[this] def areNear(character: GameCharacter, component: GameComponent) = character match {
    case mario =>
      if (character.isCloseToAComponent(component)) character.contactWithComponent(component)
      if (!character.alive) Refresh.isOver = true

    case _ => if (character.isCloseToAComponent(component)) character.contactWithComponent(component)
  }

  private[this] def lookToAllCharacters(start1: Int, stop1: Int, start2: Int, stop2: Int, component: GameComponent) = {
    for (
      j <- start1 until stop1;
      k <- start2 until stop2
    ) {
      areNear(characters.get(j), component)
      areNear(characters.get(j), characters.get(k))
      areNear(mario, characters.get(j))
    }
  }

  override def paintComponent(graphic: Graphics) = {
    super.paintComponent(graphic)
    for (i <- 0 until objects.size()) {
      this.areNear(mario, objects.get(i))
      this.lookToAllCharacters(0, nOfMushroom, nOfMushroom, nOfMushroom + nOfTurtle, objects.get(i))
      this.lookToAllCharacters(nOfMushroom, nOfMushroom + nOfTurtle, 0, nOfMushroom, objects.get(i))
    }
    for (i <- 0 until interchangeablesObject.size() - 1) {
      if (mario.isInContactWithANInterchangeableObject(interchangeablesObject.get(i))) {
        Audio.playSound(Res.AUDIO_MONEY)
        this.interchangeablesObject.remove(i)
      }
    }
    this.updateBackgroundOnMovement()
    if (_xPos >= 0 && _xPos <= 4600) {
     for (i <- 0 until  objects.size()) {
        objects.get(i).shift()
     }
     for (i <- 0 until  interchangeablesObject.size()) {
        this.interchangeablesObject.get(i).shift()
     }
     for (i <- 0 until nOfMushroom) {
        characters.get(i).shift()
      }
     for (i <- nOfMushroom until nOfMushroom + nOfTurtle) {
        characters.get(i).shift()
      }
    }

    graphic.drawImage(imageBackground1, _background1PosX, 0, null)
    graphic.drawImage(imageBackground2, _background2PosX, 0, null)
    graphic.drawImage(imageCastle, 10 - this.xPos, 95, null)
    graphic.drawImage(imageStart, 220 - this.xPos, 234, null)

    for (i <- 0 until objects.size()) {
      graphic.drawImage(objects.get(i).image, objects.get(i).x, objects.get(i).y, null)
    }
    for (i <- 0 until interchangeablesObject.size()) {
      graphic.drawImage(interchangeablesObject.get(i).changeImage, interchangeablesObject.get(i).x, interchangeablesObject.get(i).y, null)
    }

    graphic.drawImage(imgFlag, FLAG_X_POS - xPos, FLAG_Y_POS, null)
    graphic.drawImage(imgCastle, CASTLE_X_POS - xPos, CASTLE_Y_POS, null)

    mario.jumping match {
      case true => graphic.drawImage(mario.jump, mario.x, mario.y, null)
      case false => graphic.drawImage(mario.changeImage(Res.IMGP_CHARACTER_MARIO, MARIO_FREQUENCY), mario.x, mario.y, null)
    }

    for (i <- 0 until nOfMushroom + nOfTurtle) {
      characters.get(i).alive match {
        case true =>
          val img = if (characters.get(i).isInstanceOf[Mushroom]) (Res.IMGP_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY) else (Res.IMGP_CHARACTER_TURTLE, TURTLE_FREQUENCY)
          graphic.drawImage(characters.get(i).changeImage(img._1, img._2), characters.get(i).x, characters.get(i).y, null)
        case false =>
          val offset = if (characters.get(i).isInstanceOf[Mushroom]) MUSHROOM_DEAD_OFFSET_Y else TURTLE_DEAD_OFFSET_Y
          graphic.drawImage(characters.get(i).changeImageAfterDeath, characters.get(i).x, characters.get(i).y + offset, null)
          characters.get(i).moving_$eq(false)
      }
    }
  }
}