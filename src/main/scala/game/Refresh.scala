package game

import utils.Res
import javax.swing._

object Refresh {
  private val WINDOW_WIDTH = 1500
  private val WINDOW_HEIGHT = 800
  var isOver = false
}

import Refresh._
class Refresh(var finestra: JFrame) extends Runnable {
  final private val PAUSE = 3

  override def run(): Unit = {
    var alive = true
    while (alive) {
      Refresh.isOver match {
        case false =>
          Main.platform.repaint()
          try Thread.sleep(PAUSE)
          catch {
            case e: InterruptedException =>
          }
        case true =>
          finestra.setSize(Refresh.WINDOW_WIDTH, Refresh.WINDOW_HEIGHT)
          finestra.setLocationRelativeTo(null)
          val gameOver = new GameOver
          finestra.setContentPane(gameOver)
          finestra.setVisible(true)
          gameOver.repaint()
          Audio.playSound(Res.AUDIO_GAME_OVER)
          alive = false
      }
    }
  }
}