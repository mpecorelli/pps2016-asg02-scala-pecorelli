package objects

import utils.Res
import utils.Utils

object Block {
  private val WIDTH = 30
  private val HEIGHT = 30
}

class Block(posX: Int, posY: Int) extends GameObjectImpl(posX, posY, Block.WIDTH, Block.HEIGHT, Utils.getImage(Res.IMG_BLOCK)) {
}