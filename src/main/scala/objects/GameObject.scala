package objects

import component.{Component, GameComponent}
import java.awt.Image

trait GameObject extends GameComponent {

  def image: Image

  def image_= (image: Image): Unit
}

class GameObjectImpl(x: Int, y: Int, width: Int, height: Int, override var image: Image) extends Component(x, y, width, height) with GameObject {

}