package objects

import java.awt._

/**
  * Created by margherita on 15/03/17.
  */
trait ObjectInterchangeable extends GameObject {

  def changeImage: Image
}