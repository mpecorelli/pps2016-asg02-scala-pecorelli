package objects

import utils.Res
import utils.Utils
import java.awt.Image

object Piece {
  val WIDTH = 30
  val HEIGHT = 30
  val PAUSE = 10
  val FLIP_FREQUENCY = 100
}

import Piece._
class Piece(posX: Int, posY: Int) extends GameObjectImpl(posX, posY, WIDTH, HEIGHT, Utils.getImage(Res.IMG_PIECE1)) with Runnable with ObjectInterchangeable {
  private var counter = 0

  override def changeImage: Image = {
    this.counter = this.counter + 1
    Utils.getImage( if (this.counter % FLIP_FREQUENCY == 0) Res.IMG_PIECE1 else Res.IMG_PIECE2)
  }

  override def run(): Unit = while (true) {
    this.changeImage
    try Thread.sleep(PAUSE)
    catch {
      case e: InterruptedException =>
    }
  }
}