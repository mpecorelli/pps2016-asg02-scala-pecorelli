package objects

import utils.Res
import utils.Utils

object Tunnel {
  private val WIDTH = 43
  private val HEIGHT = 65
}

class Tunnel(posX: Int, posY: Int) extends GameObjectImpl(posX, posY, Tunnel.WIDTH, Tunnel.HEIGHT, Utils.getImage(Res.IMG_TUNNEL)) {
}
